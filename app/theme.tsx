import type { ThemeComponents, Theme } from '@chakra-ui/react';
import { extendTheme } from '@chakra-ui/react';

const colors = {
  primary: {
    600: '#395d93',
    500: '#417AD1',
    300: '#77a5ea',
    100: '#e3eeff',
  },
  secondary: {
    600: '#b47d2b',
    500: '#d19741',
    300: '#dbad6a',
    100: '#f9f1e5',
  },
  success: {
    600: '#1e6a48',
    500: '#25855a',
    100: '#fff5c6',
  },
  warning: {
    600: '#e8be00',
    500: '#ffd61c',
    100: '#fff5c6',
  },
  error: {
    600: '#cb2b1d',
    500: '#e0382a',
    100: '#fadedc',
  },
  gray: {
    600: '#4e5969',
    500: '#b3babe',
    300: '#e4e4e4',
    100: '#f9f9f9',
  },
  border: '#e4e4e4',
  text: '#202020',
};

const fonts = {
  body: `'Inter', sans-serif`,
  heading: `'Nunito', sans-serif`,
  mono: 'Menlo, monospace',
};

const components: ThemeComponents = {};

const styles = {};

export const theme = extendTheme({ colors, fonts, components, styles });
