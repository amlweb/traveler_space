import { Box } from '@chakra-ui/react';
import { json } from '@remix-run/node';
import { Link, Outlet, useLoaderData } from '@remix-run/react';
import db from 'app/utils/db.server';

export const loader = async () => {
  return json({
    countries: await db.country.findMany({
      orderBy: { name: 'asc' },
    }),
  });
};

export default function Countries() {
  const { countries } = useLoaderData<typeof loader>();

  return (
    <Box display='flex' margin='50px' flexDirection='column'>
      Countries List
      <Box>
        <ul>
          <li>
            <Link to='new'>Add new country</Link>
          </li>
          {countries.map((country) => (
            <li key={country.shortcut}>
              <Link to={country.shortcut}>{country.name}</Link>
            </li>
          ))}
        </ul>

        <Outlet />
      </Box>
    </Box>
  );
}
