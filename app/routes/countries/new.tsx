import db from '@Utils/db.server';
import type { ActionArgs } from '@remix-run/node';
import { json } from '@remix-run/node';
import { redirect } from '@remix-run/node';
import { Form, useActionData, useLoaderData, useTransition } from '@remix-run/react';
import { Box, Button, FormControl, FormLabel, Input, Select, VStack } from '@chakra-ui/react';

export const loader = async () => {
  return json({
    continents: await db.continent.findMany({
      select: { id: true, name: true },
    }),
  });
};

export const action = async ({ request }: ActionArgs) => {
  const form = await request.formData();
  const name = form.get('name');
  const shortcut = form.get('shortcut');
  const continent = form.get('continent');

  if (typeof shortcut !== 'string' || typeof name !== 'string' || typeof continent !== 'string') {
    return {
      error: 'error',
    };
  }

  const country = await db.country.create({
    data: {
      name,
      shortcut,
      continent: {
        connect: {
          name: continent,
        },
      },
    },
  });

  return redirect(`/countries/${country.shortcut}`);
};

export default function NewCountry() {
  const data = useLoaderData<typeof loader>();
  const transition = useTransition();

  return (
    <VStack spacing={10} width='400px'>
      <h1>Add new country</h1>
      <Form method='post'>
        <FormControl>
          <FormLabel>Name</FormLabel>
          <Input name='name' />
        </FormControl>
        <FormControl>
          <FormLabel>Shortcut</FormLabel>
          <Input name='shortcut' />
        </FormControl>
        <FormControl>
          <FormLabel>Shortcut</FormLabel>
          <Select placeholder='Select option'>
            {data.continents.map((continent) => (
              <option value={continent.name} key={continent.id}>
                {continent.name}
              </option>
            ))}
          </Select>
        </FormControl>
        <Button type='submit' isLoading={Boolean(transition.submission)}>
          Save
        </Button>
      </Form>
    </VStack>
  );
}
