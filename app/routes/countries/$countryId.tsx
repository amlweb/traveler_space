import db from '@Utils/db.server';
import type { LoaderArgs } from '@remix-run/node';
import { json } from '@remix-run/node';
import { useLoaderData } from '@remix-run/react';
import { Box } from '@chakra-ui/react';

export const loader = async ({ params }: LoaderArgs) => {
  return json({
    country: await db.country.findUnique({
      where: { shortcut: params.countryId },
    }),
  });
};

export default function Country() {
  const { country } = useLoaderData<typeof loader>();

  return (
    <Box display={'flex'} alignItems='center' justifyContent='center'>
      Selected Country {country?.name}
    </Box>
  );
}
