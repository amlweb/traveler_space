import { Box } from '@chakra-ui/react';
import { Link, Outlet } from '@remix-run/react';

export default function Index() {
  return (
    <Box display='flex' flexDirection='column' justifyContent='center' alignItems='center' margin='50px'>
      <h1>Welcome to Remix</h1>

      <Box margin='20px'>
        <Link to='signin'>Login</Link>
        <Link to='signup'>Register</Link>
      </Box>

      <Box>
        <Link to='countries'>Countries</Link>
        <Link to='travels'>Travels</Link>
      </Box>

      <Outlet />
    </Box>
  );
}
