import { Box, Button, FormControl, FormLabel, Heading, Input } from '@chakra-ui/react';
import { Form, useTransition } from '@remix-run/react';

export default function SignIn() {
  const transition = useTransition();

  return (
    <Box alignItems='center' justifyContent='center' height='100vh' textAlign='center'>
      <Box flexDirection='column' width='400px'>
        <Heading variant='h1'>Welcome to Traveler Space</Heading>
        <p>To create an account please fill the form.</p>
        <Form method='post'>
          <FormControl>
            <FormLabel>E-mail</FormLabel>
            <Input type='email' name='email' />
          </FormControl>
          <FormControl>
            <FormLabel>Password</FormLabel>
            <Input type='password' name='password' />
          </FormControl>
          <Button colorScheme='primary' type='submit' isLoading={Boolean(transition.submission)} loadingText='Sending'>
            Log in
          </Button>
        </Form>
      </Box>
    </Box>
  );
}
