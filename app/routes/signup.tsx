import { Form, useTransition } from '@remix-run/react';
import type { ActionArgs } from '@remix-run/node';
import { redirect } from '@remix-run/node';
import db from '@Utils/db.server';
import { Box, Button, FormControl, FormLabel, Input, VStack } from '@chakra-ui/react';

export const action = async ({ request }: ActionArgs) => {
  const formData = request.formData();
  const name = (await formData).get('name');
  const email = (await formData).get('email');
  const password = (await formData).get('password');

  if (typeof email !== 'string' || typeof password !== 'string' || typeof name !== 'string') {
    return {
      error: 'error',
    };
  }

  const user = await db.user.create({
    data: {
      name,
      email,
      password: password,
    },
  });

  return redirect(`/dashboard`);
};

export default function SignUp() {
  const transition = useTransition();

  return (
    <Box display={'flex'} flexDirection='column' alignItems='center' justifyContent='center' height='100vh'>
      <h1>Welcome to Traveler Space</h1>
      <p>To create an account please fill the form.</p>
      <Form method='post'>
        <VStack spacing={4} width={400} marginTop={10}>
          <FormControl>
            <FormLabel>Name</FormLabel>
            <Input name='name' />
          </FormControl>
          <FormControl>
            <FormLabel>E-mail</FormLabel>
            <Input name='email' type='email' />
          </FormControl>
          <FormControl>
            <FormLabel>Password</FormLabel>
            <Input type='password' name='password' />
          </FormControl>
          <FormControl>
            <FormLabel>Repeat password</FormLabel>
            <Input type='password' name='re-password' />
          </FormControl>
          <Button type='submit' isLoading={Boolean(transition.submission)}>
            Sign up
          </Button>
        </VStack>
      </Form>
    </Box>
  );
}
