import { Box } from '@chakra-ui/react';
import { Link, Outlet } from '@remix-run/react';

export default function Panel() {
  return (
    <Box flexDirection='column' justifyContent='center' alignItems='center' margin={50}>
      <h1>Welcome to Remix</h1>

      <Box margin={2}>
        You are logged in,
        <br />
        This is your dashboard
      </Box>

      <Outlet />
    </Box>
  );
}
