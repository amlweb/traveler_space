import { Box } from '@chakra-ui/react';
import type { LoaderArgs } from '@remix-run/node';
import { json } from '@remix-run/node';
import { useLoaderData } from '@remix-run/react';

export function loader({ params }: LoaderArgs) {
  return json({ travel: params.travel });
}

export default function Dashboard() {
  const { travel } = useLoaderData();

  return (
    <Box alignItems='center' justifyContent='center'>
      Selected Travel {travel}
    </Box>
  );
}
