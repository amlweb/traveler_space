import { Box } from '@chakra-ui/react';
import { Outlet } from '@remix-run/react';

export default function Travels() {
  return (
    <Box display={'flex'} alignItems='center' justifyContent='center'>
      Travel list
      <Outlet />
    </Box>
  );
}
