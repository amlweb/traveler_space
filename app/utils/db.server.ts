import { PrismaClient } from '@prisma/client';

let db: PrismaClient;

if (process.env.NODE_ENV === 'production') {
  db = new PrismaClient();
  console.log('Production: Created DB connection.');
} else {
    // @ts-ignore
  if (!global.prisma) {
    // @ts-ignore
    global.prisma = new PrismaClient();
    console.log('Development: Created DB connection.');
  }
// @ts-ignore
  db = global.prisma;
}

export default db;
