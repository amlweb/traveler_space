import db from 'app/utils/db.server';

export async function getUsers() {
  return db.user.findMany();
}
