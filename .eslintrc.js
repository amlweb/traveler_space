module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: ['@remix-run/eslint-config', '@remix-run/eslint-config/node', 'plugin:storybook/recommended'],
  ignorePatterns: ['*.config.js', '**/node_modules/', '**/public/'],
  overrides: [],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['react', '@typescript-eslint'],
  rules: {
    'consistent-return': 0,
    'newline-after-var': 2,
    'newline-before-return': 2,
    'object-curly-spacing': ['error', 'always'],
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 0,
    'import/prefer-default-export': 0,
    'no-console': [1, {
      allow: ['warning']
    }],
    'react/display-name': 0,
    'react/jsx-filename-extension': [2, {
      extensions: ['.js', '.jsx', '.ts', '.tsx']
    }],
    'react/jsx-props-no-spreading': 0,
    'react/prop-types': 0,
    'react/react-in-jsx-scope': 0,
    '@typescript-eslint/no-empty-function': 0,
    '@typescript-eslint/no-unused-vars': 0,
    '@typescript-eslint/no-explicit-any': 2,
    '@typescript-eslint/ban-types': 2,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    '@typescript-eslint/no-shadow': 2,
    '@typescript-eslint/no-use-before-define': 0
  },
  settings: {
    'import/resolver': 'webpack',
    'mdx/code-blocks': true,
    react: {
      version: 'detect'
    }
  }
};