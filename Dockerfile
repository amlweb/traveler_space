FROM ubuntu
USER root
WORKDIR /

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
RUN apt-get update && apt-get upgrade -y && apt-get install nodejs -y
RUN apt-get install npm -y
RUN npm install

# add app
COPY ./ .

# build remix app
RUN npm run build 
ENV NODE_ENV=production

# launch the remix app when we run this Docker image.
CMD ["npm", "run", "dev"]