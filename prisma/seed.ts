
import { PrismaClient } from '@prisma/client'
import bcrypt from 'bcrypt';
import { continentsData } from './countriesData';

const prisma = new PrismaClient();

const run = async () => {
  await Promise.all(
    continentsData.map(async (continent) => {
      return prisma.continent.upsert({
        where: { name: continent.name },
        update: {},
        create: {
          name: continent.name,
          countries: {
            create: continent.countries.map((county) => ({
              name: county.name,
              shortcut: county.shortcut,
            })),
          },
        },
      });
    }),
  );

  const salt = bcrypt.genSaltSync();
  const user = await prisma.user.upsert({
    where: { email: 'user@test.com' },
    update: {},
    create: {
      email: 'user@test.com',
      password: bcrypt.hashSync('password', salt),
      name: 'Test User',
    },
  });

  const notes = await prisma.note.findMany({})

  await Promise.all(
    new Array(10).fill(1).map(async (_, i) => {
      return prisma.travel.create({
        data: {
          title: `Travel #${i + 1}`,
          author: {
            connect: { id: user.id },
          },
          notes: {
            connect: notes.map((note) => ({
              id: note.id,
            })),
          },
        },
      })
    })
  )
};

run()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
