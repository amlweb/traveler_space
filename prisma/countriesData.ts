export const continentsData: {
    name: string,
    countries: {
        name: string,
        shortcut: string,
    }[]
}[] = [
    {
        name: 'Europe',
        countries: [
            {
                name: 'Poland',
                shortcut: 'pol'
            },
            {
                name: 'Germany',
                shortcut: 'deu'
            },
            {
                name: 'Italy',
                shortcut: 'ita'
            },
            {
                name: 'Netherlands',
                shortcut: 'ndl'
            },
            {
                name: 'Spain',
                shortcut: 'esp'
            },
        ]
    },
    {
        name: 'Africa',
        countries: [
            {
                name: 'Gambia',
                shortcut: 'gmb'
            },
            {
                name: 'Egypt',
                shortcut: 'egy'
            },
        ]
    },
    {
        name: 'Asia',
        countries: [
            {
                name: 'India',
                shortcut: 'ind'
            },
            {
                name: 'Sri Lanka',
                shortcut: 'lka'
            },
            {
                name: 'Jordan',
                shortcut: 'jor'
            },
        ]
    },
    {
        name: 'America North & South',
        countries: [
            {
                name: '	Colombia',
                shortcut: 'col'
            },
            {
                name: 'Mexico',
                shortcut: 'mex'
            },
            {
                name: 'United States of America',
                shortcut: 'usa'
            },
            {
                name: 'Panama',
                shortcut: 'pan'
            },
            {
                name: 'Canada',
                shortcut: 'can'
            },
        ]
    },
    {
        name: 'Australia & Oceania',
        countries: [
            {
                name: '	Australia',
                shortcut: 'aus'
            },
            {
                name: 'New Zealand',
                shortcut: 'nzl'
            },
            {
                name: '	Samoa',
                shortcut: 'wsm'
            },
            {
                name: 'Tonga',
                shortcut: 'ton'
            },
        ]
    },
]