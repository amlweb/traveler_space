# Welcome to Traveler Space!
The project is based on React v.18 and use the React Remix v.17 framework to handle forms in a better way.

### Quick Start
Run application by: `npm run dev` .  
Open up [http://localhost:3000](http://localhost:3000) and you should be ready to go!

### Useful links
Repository - https://gitlab.com/amlweb/traveler_space  
Jira - https://amlweb.atlassian.net/jira/software/projects/TS/boards/2/backlog

### Repo Access Rights
1. Run the SSH Agent: ```eval "$(ssh-agent)"```
2. Add the account to the SSH Agent: ```ssh-add ~/.ssh/id_rsa```
3. List added accounts ```ssh-add -l```

### Database
Postgres database is used. It is pleased on Heroku App (https://dashboard.heroku.com/apps/traveler-space-dev).
Two database are used. One as a storage of data, the second one, the shadow database id used for the communication with the first one. This is workaround.


##### Prisma
For data modeling.

```
model User {
  id        Int       @id @default(uuid())
  createdAt DateTime  @default(now())
  updatedAt DateTime  @updatedAt
  email     String    @unique
  password  String
  name      String
  countries Country[]
  travels   Travel[]
}
```

Some common comments:
```npx prisma db push``` to push the changes to the prisma seeds
```npx prisma db seed``` to execute the seeds commands
```npx prisma migrate dev``` to migrate the changes in the database schema
```npx prisma migrate reset``` to drop database and do new migration
```npx prisma studio``` to run the app on local machine http://localhost:5555/

### Styling
The Chakra UI is used for common components.
The theming is in the file theme.tsx in the app folder.

##### Storybook
All styles and the component are described at the Storybook.

Run storybook by: `npm run storybook`. 
By default it opens at [http://localhost:6006](http://localhost:6006) .

